﻿namespace AdvProgCA
{
    partial class BookingDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpBookingDetails = new System.Windows.Forms.GroupBox();
            this.cboCarForJob = new System.Windows.Forms.ComboBox();
            this.dtpStart = new System.Windows.Forms.DateTimePicker();
            this.lblStart = new System.Windows.Forms.Label();
            this.btnSaveJob = new System.Windows.Forms.Button();
            this.dtpFinish = new System.Windows.Forms.DateTimePicker();
            this.lblMechanic = new System.Windows.Forms.Label();
            this.lblCarRegNo = new System.Windows.Forms.Label();
            this.lblJobType = new System.Windows.Forms.Label();
            this.cboJobType = new System.Windows.Forms.ComboBox();
            this.cboMechanic = new System.Windows.Forms.ComboBox();
            this.lblFinish = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.lblBookingID = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.cboBookings = new System.Windows.Forms.ComboBox();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.grpBookingDetails.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpBookingDetails
            // 
            this.grpBookingDetails.Controls.Add(this.cboCarForJob);
            this.grpBookingDetails.Controls.Add(this.dtpStart);
            this.grpBookingDetails.Controls.Add(this.lblStart);
            this.grpBookingDetails.Controls.Add(this.btnSaveJob);
            this.grpBookingDetails.Controls.Add(this.dtpFinish);
            this.grpBookingDetails.Controls.Add(this.lblMechanic);
            this.grpBookingDetails.Controls.Add(this.lblCarRegNo);
            this.grpBookingDetails.Controls.Add(this.lblJobType);
            this.grpBookingDetails.Controls.Add(this.cboJobType);
            this.grpBookingDetails.Controls.Add(this.cboMechanic);
            this.grpBookingDetails.Controls.Add(this.lblFinish);
            this.grpBookingDetails.Location = new System.Drawing.Point(12, 12);
            this.grpBookingDetails.Name = "grpBookingDetails";
            this.grpBookingDetails.Size = new System.Drawing.Size(298, 226);
            this.grpBookingDetails.TabIndex = 84;
            this.grpBookingDetails.TabStop = false;
            this.grpBookingDetails.Text = "Booking Details";
            // 
            // cboCarForJob
            // 
            this.cboCarForJob.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCarForJob.FormattingEnabled = true;
            this.cboCarForJob.Location = new System.Drawing.Point(80, 58);
            this.cboCarForJob.Name = "cboCarForJob";
            this.cboCarForJob.Size = new System.Drawing.Size(200, 21);
            this.cboCarForJob.TabIndex = 2;
            // 
            // dtpStart
            // 
            this.dtpStart.Location = new System.Drawing.Point(78, 126);
            this.dtpStart.MinDate = new System.DateTime(2016, 9, 4, 16, 56, 58, 0);
            this.dtpStart.Name = "dtpStart";
            this.dtpStart.Size = new System.Drawing.Size(202, 20);
            this.dtpStart.TabIndex = 4;
            this.dtpStart.Value = new System.DateTime(2016, 9, 4, 16, 56, 58, 0);
            this.dtpStart.ValueChanged += new System.EventHandler(this.dtpStart_ValueChanged);
            // 
            // lblStart
            // 
            this.lblStart.AutoSize = true;
            this.lblStart.Location = new System.Drawing.Point(6, 126);
            this.lblStart.Name = "lblStart";
            this.lblStart.Size = new System.Drawing.Size(61, 13);
            this.lblStart.TabIndex = 84;
            this.lblStart.Text = "Start Date :";
            // 
            // btnSaveJob
            // 
            this.btnSaveJob.Location = new System.Drawing.Point(111, 184);
            this.btnSaveJob.Name = "btnSaveJob";
            this.btnSaveJob.Size = new System.Drawing.Size(135, 23);
            this.btnSaveJob.TabIndex = 6;
            this.btnSaveJob.Text = "Save Job Details";
            this.btnSaveJob.UseVisualStyleBackColor = true;
            this.btnSaveJob.Click += new System.EventHandler(this.btnSaveJob_Click);
            // 
            // dtpFinish
            // 
            this.dtpFinish.Location = new System.Drawing.Point(79, 158);
            this.dtpFinish.Name = "dtpFinish";
            this.dtpFinish.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dtpFinish.Size = new System.Drawing.Size(201, 20);
            this.dtpFinish.TabIndex = 5;
            // 
            // lblMechanic
            // 
            this.lblMechanic.AutoSize = true;
            this.lblMechanic.Location = new System.Drawing.Point(7, 94);
            this.lblMechanic.Name = "lblMechanic";
            this.lblMechanic.Size = new System.Drawing.Size(60, 13);
            this.lblMechanic.TabIndex = 54;
            this.lblMechanic.Text = "Mechanic :";
            // 
            // lblCarRegNo
            // 
            this.lblCarRegNo.AutoSize = true;
            this.lblCarRegNo.Location = new System.Drawing.Point(7, 61);
            this.lblCarRegNo.Name = "lblCarRegNo";
            this.lblCarRegNo.Size = new System.Drawing.Size(56, 13);
            this.lblCarRegNo.TabIndex = 80;
            this.lblCarRegNo.Text = "Reg. No. :";
            // 
            // lblJobType
            // 
            this.lblJobType.AutoSize = true;
            this.lblJobType.Location = new System.Drawing.Point(7, 28);
            this.lblJobType.Name = "lblJobType";
            this.lblJobType.Size = new System.Drawing.Size(57, 13);
            this.lblJobType.TabIndex = 55;
            this.lblJobType.Text = "Job Type :";
            // 
            // cboJobType
            // 
            this.cboJobType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboJobType.FormattingEnabled = true;
            this.cboJobType.Location = new System.Drawing.Point(80, 25);
            this.cboJobType.Name = "cboJobType";
            this.cboJobType.Size = new System.Drawing.Size(200, 21);
            this.cboJobType.TabIndex = 1;
            // 
            // cboMechanic
            // 
            this.cboMechanic.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboMechanic.FormattingEnabled = true;
            this.cboMechanic.Location = new System.Drawing.Point(80, 91);
            this.cboMechanic.Name = "cboMechanic";
            this.cboMechanic.Size = new System.Drawing.Size(200, 21);
            this.cboMechanic.TabIndex = 3;
            // 
            // lblFinish
            // 
            this.lblFinish.AutoSize = true;
            this.lblFinish.Location = new System.Drawing.Point(7, 158);
            this.lblFinish.Name = "lblFinish";
            this.lblFinish.Size = new System.Drawing.Size(66, 13);
            this.lblFinish.TabIndex = 75;
            this.lblFinish.Text = "Finish Date :";
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(118, 334);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(147, 21);
            this.btnDelete.TabIndex = 10;
            this.btnDelete.Text = "Delete Selected Booking";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDeleteJob_Click);
            // 
            // lblBookingID
            // 
            this.lblBookingID.AutoSize = true;
            this.lblBookingID.Location = new System.Drawing.Point(19, 256);
            this.lblBookingID.Name = "lblBookingID";
            this.lblBookingID.Size = new System.Drawing.Size(66, 13);
            this.lblBookingID.TabIndex = 68;
            this.lblBookingID.Text = "Booking ID :";
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(117, 280);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(148, 21);
            this.btnSearch.TabIndex = 8;
            this.btnSearch.Text = "Search For Booking";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearchJobs_Click);
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(12, 348);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(58, 23);
            this.btnBack.TabIndex = 11;
            this.btnBack.Text = "<< Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // cboBookings
            // 
            this.cboBookings.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBookings.FormattingEnabled = true;
            this.cboBookings.Location = new System.Drawing.Point(91, 253);
            this.cboBookings.Name = "cboBookings";
            this.cboBookings.Size = new System.Drawing.Size(201, 21);
            this.cboBookings.TabIndex = 7;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(117, 307);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(148, 21);
            this.btnUpdate.TabIndex = 9;
            this.btnUpdate.Text = "Update Selected Booking";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // BookingDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(322, 380);
            this.ControlBox = false;
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.cboBookings);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.grpBookingDetails);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.lblBookingID);
            this.Controls.Add(this.btnSearch);
            this.Name = "BookingDetails";
            this.Text = "Booking Details";
            this.grpBookingDetails.ResumeLayout(false);
            this.grpBookingDetails.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grpBookingDetails;
        private System.Windows.Forms.Label lblBookingID;
        private System.Windows.Forms.Label lblMechanic;
        private System.Windows.Forms.Label lblCarRegNo;
        private System.Windows.Forms.Label lblJobType;
        private System.Windows.Forms.ComboBox cboJobType;
        private System.Windows.Forms.ComboBox cboMechanic;
        private System.Windows.Forms.Label lblFinish;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnSaveJob;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.DateTimePicker dtpStart;
        private System.Windows.Forms.Label lblStart;
        private System.Windows.Forms.DateTimePicker dtpFinish;
        private System.Windows.Forms.ComboBox cboCarForJob;
        private System.Windows.Forms.ComboBox cboBookings;
        private System.Windows.Forms.Button btnUpdate;
    }
}