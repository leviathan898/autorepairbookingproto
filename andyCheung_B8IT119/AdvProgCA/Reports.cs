﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace AdvProgCA
{
    public partial class Reports : Form
    {
        MyMethods mm = new MyMethods();

        public Reports()
        {
            InitializeComponent();
            this.CenterToScreen();

            //load all mechanics in database to combobox
            cboMechanic.DataSource = mm.GetCurrentMechanics();

            //limit editing of records from dgv
            dgvDisplay.ReadOnly = true;
            dgvDisplay.AllowUserToAddRows = false;
            dgvDisplay.AllowUserToDeleteRows = false;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnShowAllCustomers_Click(object sender, EventArgs e)
        {
            dgvDisplay.DataSource = mm.GetCustomerList();
        }

        private void btnCustomerCars_Click(object sender, EventArgs e)
        {
            dgvDisplay.DataSource = mm.GetCustomerCarList();
        }

        private void btnJobs_Click(object sender, EventArgs e)
        {
            dgvDisplay.DataSource = mm.ShowAllBookings();
        }

        private void btnTwoWeeks_Click(object sender, EventArgs e)
        {
            //extrct mechanic ID from combobox and pass to method
            string mechanic = (Regex.Match(cboMechanic.Text, @"\d+").Value);

            dgvDisplay.DataSource = mm.Get2WeekJobs(mechanic);
        }
    }
}
