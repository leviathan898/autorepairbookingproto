﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdvProgCA
{
    class Customer : Person
    {
        public BookingMethods BookingMethod { get; set; }


        public Customer(int id, string fname, string lname, string address, string phone)
        {
            ID = id;
            FName = fname;
            LName = lname;
            Address = address;
            Phone = phone;
        }
    }
}
