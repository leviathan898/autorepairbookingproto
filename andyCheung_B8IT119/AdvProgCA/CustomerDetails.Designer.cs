﻿namespace AdvProgCA
{
    partial class CustomerDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpCarDetails = new System.Windows.Forms.GroupBox();
            this.cboCustomerCar = new System.Windows.Forms.ComboBox();
            this.cboRegNo = new System.Windows.Forms.ComboBox();
            this.cboColour = new System.Windows.Forms.ComboBox();
            this.btnSaveCar = new System.Windows.Forms.Button();
            this.lblCustomerID = new System.Windows.Forms.Label();
            this.lblRegNo = new System.Windows.Forms.Label();
            this.lblCarModel = new System.Windows.Forms.Label();
            this.txtCarModel = new System.Windows.Forms.TextBox();
            this.lblManufacturer = new System.Windows.Forms.Label();
            this.lblColour = new System.Windows.Forms.Label();
            this.txtManufacturer = new System.Windows.Forms.TextBox();
            this.btnSearchCars = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.grpCustomerDetails = new System.Windows.Forms.GroupBox();
            this.lblBookingMethod = new System.Windows.Forms.Label();
            this.rdoDropIn = new System.Windows.Forms.RadioButton();
            this.rdoPhone = new System.Windows.Forms.RadioButton();
            this.rdoEmail = new System.Windows.Forms.RadioButton();
            this.lblFName = new System.Windows.Forms.Label();
            this.btnAddCustomer = new System.Windows.Forms.Button();
            this.lblPatientLName = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.lblPhone = new System.Windows.Forms.Label();
            this.txtFName = new System.Windows.Forms.TextBox();
            this.txtLName = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.lblAddress = new System.Windows.Forms.Label();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.cboCustomers = new System.Windows.Forms.ComboBox();
            this.lblCustomers = new System.Windows.Forms.Label();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.grpCarDetails.SuspendLayout();
            this.grpCustomerDetails.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpCarDetails
            // 
            this.grpCarDetails.Controls.Add(this.cboCustomerCar);
            this.grpCarDetails.Controls.Add(this.cboRegNo);
            this.grpCarDetails.Controls.Add(this.cboColour);
            this.grpCarDetails.Controls.Add(this.btnSaveCar);
            this.grpCarDetails.Controls.Add(this.lblCustomerID);
            this.grpCarDetails.Controls.Add(this.lblRegNo);
            this.grpCarDetails.Controls.Add(this.lblCarModel);
            this.grpCarDetails.Controls.Add(this.txtCarModel);
            this.grpCarDetails.Controls.Add(this.lblManufacturer);
            this.grpCarDetails.Controls.Add(this.lblColour);
            this.grpCarDetails.Controls.Add(this.txtManufacturer);
            this.grpCarDetails.Controls.Add(this.btnSearchCars);
            this.grpCarDetails.Location = new System.Drawing.Point(301, 14);
            this.grpCarDetails.Name = "grpCarDetails";
            this.grpCarDetails.Size = new System.Drawing.Size(238, 275);
            this.grpCarDetails.TabIndex = 85;
            this.grpCarDetails.TabStop = false;
            this.grpCarDetails.Text = "Car Details";
            // 
            // cboCustomerCar
            // 
            this.cboCustomerCar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCustomerCar.FormattingEnabled = true;
            this.cboCustomerCar.Location = new System.Drawing.Point(92, 107);
            this.cboCustomerCar.Name = "cboCustomerCar";
            this.cboCustomerCar.Size = new System.Drawing.Size(140, 21);
            this.cboCustomerCar.TabIndex = 12;
            // 
            // cboRegNo
            // 
            this.cboRegNo.FormattingEnabled = true;
            this.cboRegNo.Location = new System.Drawing.Point(92, 24);
            this.cboRegNo.Name = "cboRegNo";
            this.cboRegNo.Size = new System.Drawing.Size(140, 21);
            this.cboRegNo.TabIndex = 10;
            // 
            // cboColour
            // 
            this.cboColour.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboColour.FormattingEnabled = true;
            this.cboColour.Location = new System.Drawing.Point(92, 207);
            this.cboColour.Name = "cboColour";
            this.cboColour.Size = new System.Drawing.Size(140, 21);
            this.cboColour.TabIndex = 15;
            // 
            // btnSaveCar
            // 
            this.btnSaveCar.Location = new System.Drawing.Point(101, 236);
            this.btnSaveCar.Name = "btnSaveCar";
            this.btnSaveCar.Size = new System.Drawing.Size(124, 23);
            this.btnSaveCar.TabIndex = 16;
            this.btnSaveCar.Text = "Save Car Details";
            this.btnSaveCar.UseVisualStyleBackColor = true;
            this.btnSaveCar.Click += new System.EventHandler(this.btnSaveCar_Click);
            // 
            // lblCustomerID
            // 
            this.lblCustomerID.AutoSize = true;
            this.lblCustomerID.Location = new System.Drawing.Point(6, 110);
            this.lblCustomerID.Name = "lblCustomerID";
            this.lblCustomerID.Size = new System.Drawing.Size(71, 13);
            this.lblCustomerID.TabIndex = 67;
            this.lblCustomerID.Text = "Customer ID :";
            // 
            // lblRegNo
            // 
            this.lblRegNo.AutoSize = true;
            this.lblRegNo.Location = new System.Drawing.Point(6, 27);
            this.lblRegNo.Name = "lblRegNo";
            this.lblRegNo.Size = new System.Drawing.Size(56, 13);
            this.lblRegNo.TabIndex = 60;
            this.lblRegNo.Text = "Reg. No. :";
            // 
            // lblCarModel
            // 
            this.lblCarModel.AutoSize = true;
            this.lblCarModel.Location = new System.Drawing.Point(6, 178);
            this.lblCarModel.Name = "lblCarModel";
            this.lblCarModel.Size = new System.Drawing.Size(61, 13);
            this.lblCarModel.TabIndex = 62;
            this.lblCarModel.Text = "Car Model :";
            // 
            // txtCarModel
            // 
            this.txtCarModel.Location = new System.Drawing.Point(92, 175);
            this.txtCarModel.Name = "txtCarModel";
            this.txtCarModel.Size = new System.Drawing.Size(140, 20);
            this.txtCarModel.TabIndex = 14;
            // 
            // lblManufacturer
            // 
            this.lblManufacturer.AutoSize = true;
            this.lblManufacturer.Location = new System.Drawing.Point(6, 144);
            this.lblManufacturer.Name = "lblManufacturer";
            this.lblManufacturer.Size = new System.Drawing.Size(76, 13);
            this.lblManufacturer.TabIndex = 70;
            this.lblManufacturer.Text = "Manufacturer :";
            // 
            // lblColour
            // 
            this.lblColour.AutoSize = true;
            this.lblColour.Location = new System.Drawing.Point(6, 210);
            this.lblColour.Name = "lblColour";
            this.lblColour.Size = new System.Drawing.Size(43, 13);
            this.lblColour.TabIndex = 73;
            this.lblColour.Text = "Colour :";
            // 
            // txtManufacturer
            // 
            this.txtManufacturer.Location = new System.Drawing.Point(92, 141);
            this.txtManufacturer.Name = "txtManufacturer";
            this.txtManufacturer.Size = new System.Drawing.Size(140, 20);
            this.txtManufacturer.TabIndex = 13;
            // 
            // btnSearchCars
            // 
            this.btnSearchCars.Location = new System.Drawing.Point(111, 51);
            this.btnSearchCars.Name = "btnSearchCars";
            this.btnSearchCars.Size = new System.Drawing.Size(100, 23);
            this.btnSearchCars.TabIndex = 11;
            this.btnSearchCars.Text = "Load Car Details";
            this.btnSearchCars.UseVisualStyleBackColor = true;
            this.btnSearchCars.Click += new System.EventHandler(this.btnSearchCars_Click);
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(12, 379);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(58, 23);
            this.btnBack.TabIndex = 17;
            this.btnBack.Text = "<< Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // grpCustomerDetails
            // 
            this.grpCustomerDetails.Controls.Add(this.btnUpdate);
            this.grpCustomerDetails.Controls.Add(this.cboCustomers);
            this.grpCustomerDetails.Controls.Add(this.btnSearch);
            this.grpCustomerDetails.Controls.Add(this.lblCustomers);
            this.grpCustomerDetails.Controls.Add(this.lblBookingMethod);
            this.grpCustomerDetails.Controls.Add(this.rdoDropIn);
            this.grpCustomerDetails.Controls.Add(this.rdoPhone);
            this.grpCustomerDetails.Controls.Add(this.rdoEmail);
            this.grpCustomerDetails.Controls.Add(this.lblFName);
            this.grpCustomerDetails.Controls.Add(this.btnAddCustomer);
            this.grpCustomerDetails.Controls.Add(this.lblPatientLName);
            this.grpCustomerDetails.Controls.Add(this.lblEmail);
            this.grpCustomerDetails.Controls.Add(this.lblPhone);
            this.grpCustomerDetails.Controls.Add(this.txtFName);
            this.grpCustomerDetails.Controls.Add(this.txtLName);
            this.grpCustomerDetails.Controls.Add(this.txtEmail);
            this.grpCustomerDetails.Controls.Add(this.txtPhone);
            this.grpCustomerDetails.Controls.Add(this.lblAddress);
            this.grpCustomerDetails.Controls.Add(this.txtAddress);
            this.grpCustomerDetails.Location = new System.Drawing.Point(24, 14);
            this.grpCustomerDetails.Name = "grpCustomerDetails";
            this.grpCustomerDetails.Size = new System.Drawing.Size(271, 346);
            this.grpCustomerDetails.TabIndex = 83;
            this.grpCustomerDetails.TabStop = false;
            this.grpCustomerDetails.Text = "Customer Details";
            // 
            // lblBookingMethod
            // 
            this.lblBookingMethod.AutoSize = true;
            this.lblBookingMethod.Location = new System.Drawing.Point(24, 190);
            this.lblBookingMethod.Name = "lblBookingMethod";
            this.lblBookingMethod.Size = new System.Drawing.Size(91, 13);
            this.lblBookingMethod.TabIndex = 69;
            this.lblBookingMethod.Text = "Booking Method :";
            // 
            // rdoDropIn
            // 
            this.rdoDropIn.AutoSize = true;
            this.rdoDropIn.Location = new System.Drawing.Point(145, 206);
            this.rdoDropIn.Name = "rdoDropIn";
            this.rdoDropIn.Size = new System.Drawing.Size(57, 17);
            this.rdoDropIn.TabIndex = 8;
            this.rdoDropIn.TabStop = true;
            this.rdoDropIn.Text = "DropIn";
            this.rdoDropIn.UseVisualStyleBackColor = true;
            // 
            // rdoPhone
            // 
            this.rdoPhone.AutoSize = true;
            this.rdoPhone.Location = new System.Drawing.Point(27, 206);
            this.rdoPhone.Name = "rdoPhone";
            this.rdoPhone.Size = new System.Drawing.Size(56, 17);
            this.rdoPhone.TabIndex = 6;
            this.rdoPhone.TabStop = true;
            this.rdoPhone.Text = "Phone";
            this.rdoPhone.UseVisualStyleBackColor = true;
            // 
            // rdoEmail
            // 
            this.rdoEmail.AutoSize = true;
            this.rdoEmail.Location = new System.Drawing.Point(89, 206);
            this.rdoEmail.Name = "rdoEmail";
            this.rdoEmail.Size = new System.Drawing.Size(50, 17);
            this.rdoEmail.TabIndex = 7;
            this.rdoEmail.TabStop = true;
            this.rdoEmail.Text = "Email";
            this.rdoEmail.UseVisualStyleBackColor = true;
            // 
            // lblFName
            // 
            this.lblFName.AutoSize = true;
            this.lblFName.Location = new System.Drawing.Point(24, 24);
            this.lblFName.Name = "lblFName";
            this.lblFName.Size = new System.Drawing.Size(63, 13);
            this.lblFName.TabIndex = 41;
            this.lblFName.Text = "First Name :";
            // 
            // btnAddCustomer
            // 
            this.btnAddCustomer.Location = new System.Drawing.Point(6, 236);
            this.btnAddCustomer.Name = "btnAddCustomer";
            this.btnAddCustomer.Size = new System.Drawing.Size(124, 23);
            this.btnAddCustomer.TabIndex = 9;
            this.btnAddCustomer.Text = "Add New Customer";
            this.btnAddCustomer.UseVisualStyleBackColor = true;
            this.btnAddCustomer.Click += new System.EventHandler(this.btnAddCustomer_Click);
            // 
            // lblPatientLName
            // 
            this.lblPatientLName.AutoSize = true;
            this.lblPatientLName.Location = new System.Drawing.Point(24, 57);
            this.lblPatientLName.Name = "lblPatientLName";
            this.lblPatientLName.Size = new System.Drawing.Size(64, 13);
            this.lblPatientLName.TabIndex = 43;
            this.lblPatientLName.Text = "Last Name :";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(24, 124);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(38, 13);
            this.lblEmail.TabIndex = 47;
            this.lblEmail.Text = "Email :";
            // 
            // lblPhone
            // 
            this.lblPhone.AutoSize = true;
            this.lblPhone.Location = new System.Drawing.Point(24, 158);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(44, 13);
            this.lblPhone.TabIndex = 48;
            this.lblPhone.Text = "Phone :";
            // 
            // txtFName
            // 
            this.txtFName.Location = new System.Drawing.Point(104, 21);
            this.txtFName.Name = "txtFName";
            this.txtFName.Size = new System.Drawing.Size(100, 20);
            this.txtFName.TabIndex = 1;
            // 
            // txtLName
            // 
            this.txtLName.Location = new System.Drawing.Point(104, 54);
            this.txtLName.Name = "txtLName";
            this.txtLName.Size = new System.Drawing.Size(100, 20);
            this.txtLName.TabIndex = 2;
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(104, 121);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(100, 20);
            this.txtEmail.TabIndex = 4;
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(104, 155);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(100, 20);
            this.txtPhone.TabIndex = 5;
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Location = new System.Drawing.Point(24, 90);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(51, 13);
            this.lblAddress.TabIndex = 52;
            this.lblAddress.Text = "Address :";
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(104, 87);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(100, 20);
            this.txtAddress.TabIndex = 3;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(104, 305);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(124, 23);
            this.btnSearch.TabIndex = 70;
            this.btnSearch.Text = "Search by Customer ID";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // cboCustomers
            // 
            this.cboCustomers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCustomers.FormattingEnabled = true;
            this.cboCustomers.Location = new System.Drawing.Point(98, 271);
            this.cboCustomers.Name = "cboCustomers";
            this.cboCustomers.Size = new System.Drawing.Size(140, 21);
            this.cboCustomers.TabIndex = 74;
            // 
            // lblCustomers
            // 
            this.lblCustomers.AutoSize = true;
            this.lblCustomers.Location = new System.Drawing.Point(12, 274);
            this.lblCustomers.Name = "lblCustomers";
            this.lblCustomers.Size = new System.Drawing.Size(71, 13);
            this.lblCustomers.TabIndex = 75;
            this.lblCustomers.Text = "Customer ID :";
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(141, 236);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(124, 23);
            this.btnUpdate.TabIndex = 76;
            this.btnUpdate.Text = "Update Customer";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // CustomerDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(560, 414);
            this.ControlBox = false;
            this.Controls.Add(this.grpCustomerDetails);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.grpCarDetails);
            this.Name = "CustomerDetails";
            this.Text = "Customer Details";
            this.grpCarDetails.ResumeLayout(false);
            this.grpCarDetails.PerformLayout();
            this.grpCustomerDetails.ResumeLayout(false);
            this.grpCustomerDetails.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpCarDetails;
        private System.Windows.Forms.Label lblCustomerID;
        private System.Windows.Forms.Label lblRegNo;
        private System.Windows.Forms.Label lblCarModel;
        private System.Windows.Forms.Button btnSaveCar;
        private System.Windows.Forms.TextBox txtCarModel;
        private System.Windows.Forms.Label lblManufacturer;
        private System.Windows.Forms.Label lblColour;
        private System.Windows.Forms.TextBox txtManufacturer;
        private System.Windows.Forms.Button btnSearchCars;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.ComboBox cboColour;
        private System.Windows.Forms.ComboBox cboRegNo;
        private System.Windows.Forms.GroupBox grpCustomerDetails;
        private System.Windows.Forms.Label lblBookingMethod;
        private System.Windows.Forms.RadioButton rdoDropIn;
        private System.Windows.Forms.RadioButton rdoPhone;
        private System.Windows.Forms.RadioButton rdoEmail;
        private System.Windows.Forms.Label lblFName;
        private System.Windows.Forms.Label lblPatientLName;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lblPhone;
        private System.Windows.Forms.TextBox txtFName;
        private System.Windows.Forms.TextBox txtLName;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.Button btnAddCustomer;
        private System.Windows.Forms.ComboBox cboCustomerCar;
        private System.Windows.Forms.ComboBox cboCustomers;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label lblCustomers;
        private System.Windows.Forms.Button btnUpdate;
    }
}