﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Text.RegularExpressions;

namespace AdvProgCA
{
    public partial class CustomerDetails : Form
    {
        MyMethods mm = new MyMethods();
        string checkRegRegex = ConfigurationManager.AppSettings["RegRegex"];
        string noDigitsRegex = ConfigurationManager.AppSettings["NoDigits"];
        string mobileRegex = ConfigurationManager.AppSettings["MobileRegEx"];
        string emailRegex = ConfigurationManager.AppSettings["EmailRegex"];

        public CustomerDetails()
        {
            InitializeComponent();
            this.CenterToScreen();

            //populate comboboxes with data (enums, methods/queries to database)
            cboColour.DataSource = Enum.GetValues(typeof(Classes.CarColour));
            cboRegNo.DataSource = mm.GetExistingCars();
            cboCustomerCar.DataSource = mm.GetExistingCustomers();
            cboCustomers.DataSource = mm.GetExistingCustomers();
            cboRegNo.SelectedIndex = -1;
            cboCustomerCar.SelectedIndex = -1;
            btnUpdate.Enabled = false;
            rdoDropIn.Select();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSaveCar_Click(object sender, EventArgs e)
        {
            string regNo = cboRegNo.Text;
            string manufacturer = txtManufacturer.Text;
            string model = txtCarModel.Text;

            //check if reg no. matches Regex (for Irish plates)
            if (!(Regex.IsMatch(regNo, checkRegRegex)))
            {
                MessageBox.Show("Invalid Registration Plate Number.");
            }
            else if (!(Regex.IsMatch(manufacturer, noDigitsRegex) || Regex.IsMatch(model, noDigitsRegex)))
            {
                MessageBox.Show("Only non-numbers allowed for Manufacturer and Model fields.");
            }
            else
            {
                //extract customer ID number from combobox text
                string customerID = Regex.Match(cboCustomerCar.Text, @"\d+").Value;
                string colour = cboColour.Text;

                MessageBox.Show(mm.SaveCar(regNo, customerID, manufacturer, model, colour));

                this.Close();

                //cmbRegNo.SelectedIndex = -1;
                //cmbCustomerCar.SelectedIndex = -1;
                //txtManufacturer.Clear();
                //txtCarModel.Clear();
            }
        }

        private void btnAddCustomer_Click(object sender, EventArgs e)
        {
            string bookingMethod;
            
            if (rdoDropIn.Checked)
            {
                bookingMethod = rdoDropIn.Text;
            }
            else if(rdoEmail.Checked)
            {
                bookingMethod = rdoEmail.Text;
            }
            else
            {
                bookingMethod = rdoPhone.Text;
            }


            string fName = txtFName.Text;
            string lName = txtLName.Text;
            string address = txtAddress.Text;
            string phone = txtPhone.Text;
            string email = txtEmail.Text;

            if (!(Regex.IsMatch(fName, noDigitsRegex) || Regex.IsMatch(lName, noDigitsRegex)))
            {
                MessageBox.Show("Invalid Name inputs. Only non-numbers are allowed.");
            }
            else if (!(Regex.IsMatch(phone, mobileRegex)))
            {
                MessageBox.Show("Invalid Contact Information. Only numbers are allowed for phones.");
            }
            else
            {
                MessageBox.Show(mm.AddCustomer(fName, lName, address, phone, email, bookingMethod));
                this.Close();
            }

            //txtFName.Clear();
            //txtLName.Clear();
            //txtAddress.Clear();
            //txtPhone.Clear();
            //txtEmail.Clear();
            //rdoDropIn.Select();

        }

        private void btnSearchCars_Click(object sender, EventArgs e)
        {
            //load car details based on reg no. input
            Classes.CarClass selectedCar = mm.GetSelectedCar(cboRegNo.Text);

            string carsCustomer = selectedCar.CustomerID.ToString();

            //match customer of selected car with customers listed in combobox and select them
            foreach (var item in cboCustomerCar.Items)
            {
                if ((item.ToString().Contains(carsCustomer)) && item.ToString().IndexOf(carsCustomer) == item.ToString().LastIndexOf(carsCustomer))
                {
                    cboCustomerCar.SelectedItem = item;
                }
            }

            //load searched for car details into form fields
            cboRegNo.Text = selectedCar.RegNo;
            txtCarModel.Text = selectedCar.Model;
            txtManufacturer.Text = selectedCar.Manufacturer;
            txtCarModel.Text = selectedCar.Model;
            cboColour.SelectedItem = selectedCar.Colour;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            //disable add button to prevent duplicate records for same customer
            btnAddCustomer.Enabled = false;
            btnUpdate.Enabled = true;

            //match customer with extracted ID from cbo, pass to method
            Classes.CustomerClass selectedCustomer = mm.GetSelectedCustomer(Regex.Match(cboCustomers.Text, @"\d+").Value);

            //fill fields with searched customer details
            txtFName.Text = selectedCustomer.FName;
            txtLName.Text = selectedCustomer.LName;
            txtAddress.Text = selectedCustomer.Address;
            txtEmail.Text = selectedCustomer.Email;
            txtPhone.Text = selectedCustomer.Phone;

            if (rdoDropIn.Text == selectedCustomer.BookingMethod.ToString())
            {
                rdoDropIn.Select();
            }
            else if (rdoEmail.Text == selectedCustomer.BookingMethod.ToString())
            {
                rdoEmail.Select();
            }
            else
            {
                rdoPhone.Select();
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            string bookingMethod;

            if (rdoDropIn.Checked)
            {
                bookingMethod = rdoDropIn.Text;
            }
            else if (rdoEmail.Checked)
            {
                bookingMethod = rdoEmail.Text;
            }
            else
            {
                bookingMethod = rdoPhone.Text;
            }

            string customerID = Regex.Match(cboCustomers.Text, @"\d+").Value;
            string fName = txtFName.Text;
            string lName = txtLName.Text;
            string address = txtAddress.Text;
            string phone = txtPhone.Text;
            string email = txtEmail.Text;

            if (!(Regex.IsMatch(fName, noDigitsRegex) || Regex.IsMatch(lName, noDigitsRegex)))
            {
                MessageBox.Show("Invalid Name inputs. Only non-numbers are allowed");
            }
            else if (!(Regex.IsMatch(phone, mobileRegex)))
            {
                MessageBox.Show("Invalid Contact Information. Only numbers are allowed for phone.");
            }
            else
            {
                MessageBox.Show(mm.UpdateCustomer(customerID, fName, lName, address, phone, email, bookingMethod));
            }

            this.Close();
        }
    }
}
