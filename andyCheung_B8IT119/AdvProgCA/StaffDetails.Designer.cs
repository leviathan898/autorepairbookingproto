﻿namespace AdvProgCA
{
    partial class StaffDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpStaffDetails = new System.Windows.Forms.GroupBox();
            this.lblPassword = new System.Windows.Forms.Label();
            this.cboStaffType = new System.Windows.Forms.ComboBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.lblStaffType = new System.Windows.Forms.Label();
            this.lblFName = new System.Windows.Forms.Label();
            this.btnAddStaff = new System.Windows.Forms.Button();
            this.lblPatientLName = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.lblPhone = new System.Windows.Forms.Label();
            this.txtFName = new System.Windows.Forms.TextBox();
            this.txtLName = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.lblAddress = new System.Windows.Forms.Label();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.btnBack = new System.Windows.Forms.Button();
            this.grpStaffDetails.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpStaffDetails
            // 
            this.grpStaffDetails.Controls.Add(this.lblPassword);
            this.grpStaffDetails.Controls.Add(this.cboStaffType);
            this.grpStaffDetails.Controls.Add(this.txtPassword);
            this.grpStaffDetails.Controls.Add(this.lblStaffType);
            this.grpStaffDetails.Controls.Add(this.lblFName);
            this.grpStaffDetails.Controls.Add(this.btnAddStaff);
            this.grpStaffDetails.Controls.Add(this.lblPatientLName);
            this.grpStaffDetails.Controls.Add(this.lblEmail);
            this.grpStaffDetails.Controls.Add(this.lblPhone);
            this.grpStaffDetails.Controls.Add(this.txtFName);
            this.grpStaffDetails.Controls.Add(this.txtLName);
            this.grpStaffDetails.Controls.Add(this.txtEmail);
            this.grpStaffDetails.Controls.Add(this.txtPhone);
            this.grpStaffDetails.Controls.Add(this.lblAddress);
            this.grpStaffDetails.Controls.Add(this.txtAddress);
            this.grpStaffDetails.Location = new System.Drawing.Point(12, 12);
            this.grpStaffDetails.Name = "grpStaffDetails";
            this.grpStaffDetails.Size = new System.Drawing.Size(455, 179);
            this.grpStaffDetails.TabIndex = 85;
            this.grpStaffDetails.TabStop = false;
            this.grpStaffDetails.Text = "Staff Details";
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(18, 133);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(59, 13);
            this.lblPassword.TabIndex = 68;
            this.lblPassword.Text = "Password :";
            // 
            // cboStaffType
            // 
            this.cboStaffType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboStaffType.FormattingEnabled = true;
            this.cboStaffType.Location = new System.Drawing.Point(340, 99);
            this.cboStaffType.Name = "cboStaffType";
            this.cboStaffType.Size = new System.Drawing.Size(100, 21);
            this.cboStaffType.TabIndex = 7;
            this.cboStaffType.Tag = "";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(111, 130);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(100, 20);
            this.txtPassword.TabIndex = 4;
            this.txtPassword.Tag = "";
            // 
            // lblStaffType
            // 
            this.lblStaffType.AutoSize = true;
            this.lblStaffType.Location = new System.Drawing.Point(247, 99);
            this.lblStaffType.Name = "lblStaffType";
            this.lblStaffType.Size = new System.Drawing.Size(62, 13);
            this.lblStaffType.TabIndex = 66;
            this.lblStaffType.Text = "Staff Type :";
            // 
            // lblFName
            // 
            this.lblFName.AutoSize = true;
            this.lblFName.Location = new System.Drawing.Point(18, 30);
            this.lblFName.Name = "lblFName";
            this.lblFName.Size = new System.Drawing.Size(63, 13);
            this.lblFName.TabIndex = 41;
            this.lblFName.Text = "First Name :";
            // 
            // btnAddStaff
            // 
            this.btnAddStaff.Location = new System.Drawing.Point(270, 134);
            this.btnAddStaff.Name = "btnAddStaff";
            this.btnAddStaff.Size = new System.Drawing.Size(151, 23);
            this.btnAddStaff.TabIndex = 8;
            this.btnAddStaff.Tag = "";
            this.btnAddStaff.Text = "Add Staff Account";
            this.btnAddStaff.UseVisualStyleBackColor = true;
            this.btnAddStaff.Click += new System.EventHandler(this.btnAddStaff_Click);
            // 
            // lblPatientLName
            // 
            this.lblPatientLName.AutoSize = true;
            this.lblPatientLName.Location = new System.Drawing.Point(18, 63);
            this.lblPatientLName.Name = "lblPatientLName";
            this.lblPatientLName.Size = new System.Drawing.Size(64, 13);
            this.lblPatientLName.TabIndex = 43;
            this.lblPatientLName.Text = "Last Name :";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(247, 30);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(38, 13);
            this.lblEmail.TabIndex = 47;
            this.lblEmail.Text = "Email :";
            // 
            // lblPhone
            // 
            this.lblPhone.AutoSize = true;
            this.lblPhone.Location = new System.Drawing.Point(247, 64);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(44, 13);
            this.lblPhone.TabIndex = 48;
            this.lblPhone.Text = "Phone :";
            // 
            // txtFName
            // 
            this.txtFName.Location = new System.Drawing.Point(111, 27);
            this.txtFName.Name = "txtFName";
            this.txtFName.Size = new System.Drawing.Size(100, 20);
            this.txtFName.TabIndex = 1;
            this.txtFName.Tag = "";
            // 
            // txtLName
            // 
            this.txtLName.Location = new System.Drawing.Point(111, 60);
            this.txtLName.Name = "txtLName";
            this.txtLName.Size = new System.Drawing.Size(100, 20);
            this.txtLName.TabIndex = 2;
            this.txtLName.Tag = "";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(340, 27);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(100, 20);
            this.txtEmail.TabIndex = 5;
            this.txtEmail.Tag = "";
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(340, 61);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(100, 20);
            this.txtPhone.TabIndex = 6;
            this.txtPhone.Tag = "";
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Location = new System.Drawing.Point(18, 99);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(51, 13);
            this.lblAddress.TabIndex = 52;
            this.lblAddress.Text = "Address :";
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(111, 96);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(100, 20);
            this.txtAddress.TabIndex = 3;
            this.txtAddress.Tag = "";
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(12, 216);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(58, 23);
            this.btnBack.TabIndex = 9;
            this.btnBack.Tag = "";
            this.btnBack.Text = "<< Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // StaffDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(481, 251);
            this.ControlBox = false;
            this.Controls.Add(this.grpStaffDetails);
            this.Controls.Add(this.btnBack);
            this.Name = "StaffDetails";
            this.Text = "Staff Details";
            this.grpStaffDetails.ResumeLayout(false);
            this.grpStaffDetails.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox grpStaffDetails;
        private System.Windows.Forms.Label lblFName;
        private System.Windows.Forms.Button btnAddStaff;
        private System.Windows.Forms.Label lblPatientLName;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lblPhone;
        private System.Windows.Forms.TextBox txtFName;
        private System.Windows.Forms.TextBox txtLName;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Label lblStaffType;
        private System.Windows.Forms.ComboBox cboStaffType;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.TextBox txtPassword;
    }
}