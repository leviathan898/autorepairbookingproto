﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace AdvProgCA
{
    public partial class BookingDetails : Form
    {
        MyMethods mm = new MyMethods();

        public BookingDetails()
        {
            InitializeComponent();
            this.CenterToScreen();

            //load comboboxes with job types and existing database records (mechanics, cars, bookings)
            cboJobType.DataSource = Enum.GetValues(typeof(Classes.JobTypes));

            cboMechanic.DataSource = mm.GetCurrentMechanics();
            cboCarForJob.DataSource = mm.GetExistingCars();
            cboBookings.DataSource = mm.GetExistingBookings();
            
            //format date values and set min date
            dtpStart.CustomFormat = "dd'/'MM'/'yyyy";
            dtpStart.Format = DateTimePickerFormat.Custom;
            dtpStart.MinDate = new DateTime(1985, 6, 20);

            dtpFinish.CustomFormat = "dd'/'MM'/'yyyy";
            dtpFinish.Format = DateTimePickerFormat.Custom;

            btnUpdate.Enabled = false;
            btnDelete.Enabled = false;
        }

        private void dtpStart_ValueChanged(object sender, EventArgs e)
        {
            //if start date value is selected, set min finish date = start date
            dtpFinish.MinDate = dtpStart.Value;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSaveJob_Click(object sender, EventArgs e)
        {
            int jobID = (cboJobType.SelectedIndex)+1;
            string regNo = cboCarForJob.Text;
            //extract mechanic ID number from combobox text
            string mechanic = (Regex.Match(cboMechanic.Text, @"\d+").Value);
            DateTime startDate = dtpStart.Value.Date;
            DateTime finishDate = dtpFinish.Value.Date;

            MessageBox.Show(mm.SaveJob(jobID, regNo, mechanic, startDate, finishDate));

            this.Close();
        }

        private void btnSearchJobs_Click(object sender, EventArgs e)
        {
            //extract booking ID from combobox and pass to method
            Classes.Bookings selectedBooking = mm.GetSelectedBooking(Regex.Match(cboBookings.Text, @"\d+").Value);

            //if a booking is found, load details of it into form fields, disable add job button and only allow editing and/or deleting of the record
            if(selectedBooking!=null)
            {
                cboJobType.SelectedIndex = (selectedBooking.JobID) - 1;
                cboCarForJob.Text = selectedBooking.RegNo;
                cboMechanic.SelectedIndex = cboMechanic.FindString(selectedBooking.StaffID.ToString());
                dtpStart.Value = selectedBooking.StartDate;
                dtpFinish.Value = selectedBooking.FinishDate;

                btnSaveJob.Enabled = false;
                btnUpdate.Enabled = true;
                btnDelete.Enabled = true;
            }
        }

        private void btnDeleteJob_Click(object sender, EventArgs e)
        {
            //delete booking selected in combobox based on extracted ID
            MessageBox.Show(mm.DeleteBooking(Regex.Match(cboBookings.Text, @"\d+").Value));
            this.Close();
        }

        

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            //edit bookings and save new details
            int jobID = (cboJobType.SelectedIndex) + 1;
            string regNo = cboCarForJob.Text;
            int mechanic = int.Parse(Regex.Match(cboMechanic.Text, @"\d+").Value);
            DateTime startDate = dtpStart.Value.Date;
            DateTime finishDate = dtpFinish.Value.Date;

            MessageBox.Show(mm.UpdateBooking(Regex.Match(cboBookings.Text, @"\d+").Value, jobID, regNo, mechanic, startDate, finishDate));
            this.Close();
        }
    }
}
