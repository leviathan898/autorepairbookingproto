﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdvProgCA.Classes
{
    //enums for comboboxes/radio buttons
    public enum BookingMethods
    {
        Phone,
        Email,
        DropIn
    }

    public enum StaffTypes
    {
        Mechanic,
        Support,
        Admin
    }

    public enum JobTypes
    {
        FullService = 1,
        MinorService,
        PreNCT,
        ReplaceTyres,
        VehicleRecovery,
        WindscreenReplace,
        TimingBeltReplace,
        CVBootReplace,
        AxleRealign,
        BodyShopRepair,
        EngineRepair,
        GeneralRepair
    }

    public enum CarColour
    {
        White,
        Silver,
        Black,
        Grey,
        Blue,
        Red,
        Brown,
        Green,
        Other
    }
}
