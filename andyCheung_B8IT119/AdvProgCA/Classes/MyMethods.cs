﻿using AdvProgCA.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace AdvProgCA
{
    class MyMethods
    {
        //method to check if any staff exists in database
        public bool checkForExistingStaff()
        {
            using (AutoJalopyL2SDataContext autoJDataContext = new AutoJalopyL2SDataContext())
            {
                bool checkForStaff = (from staffMembers in autoJDataContext.tblStaffs
                                     select staffMembers).Any();
                
                return checkForStaff;
            }
        }

        //method for logging in
        public void Login(string id, string password, out bool checkAuthentication, out StaffTypes loggingInStaff)
        {
            int parsedID;
            checkAuthentication = int.TryParse(id, out parsedID); //check if ID is a number
            loggingInStaff = 0;

            //if ID is a number
            if (checkAuthentication)
            {
                using (AutoJalopyL2SDataContext autoJDataContext = new AutoJalopyL2SDataContext())
                {
                    try
                    {
                        //match ID with staff ID
                        loggingInStaff = (StaffTypes)Enum.Parse(typeof(StaffTypes),(from staff in autoJDataContext.tblStaffs
                                              where staff.staffID == parsedID
                                              && staff.passwordHash == password
                                              select staff.staffType).Single());
                    }
                    catch (Exception)
                    {
                        // if ID doesn't exist
                        checkAuthentication = false;
                    }
                }
            }
        }

        //method for adding new staff
        public string AddStaff(string fn, string ln, string addrss, string phn, string eml, string psswrd, string stffTyp)
        {
            string addStaffResultMessage = "";

            using (AutoJalopyL2SDataContext autoJDC = new AutoJalopyL2SDataContext())
            {
                try
                {
                        tblStaff newStaff = new tblStaff
                    {
                        firstName = fn,
                        lastName = ln,
                        staffAddress = addrss,
                        phone = phn,
                        email = eml,
                        passwordHash = psswrd,
                        staffType = stffTyp
                    };

                    autoJDC.tblStaffs.InsertOnSubmit(newStaff);
                    autoJDC.SubmitChanges();

                    addStaffResultMessage = "New Staff account created.";
                }

                catch
                {
                    addStaffResultMessage = "Staff registration failed. Please ensure all fields are entered correctly.";
                }
            }
            

            return addStaffResultMessage;
        }

        //method for adding new car or updating car
        public string SaveCar(string regNo, string customerId, string manufacturer, string model, string colour)
        {
            string addCarResultMessage = "";

            using (AutoJalopyL2SDataContext autoJDC = new AutoJalopyL2SDataContext())
            {
                int custID;

                int.TryParse(customerId, out custID);

                try
                {
                    //check if registration plate number exists already
                    var existingCar = (from car in autoJDC.tblCars
                                       where regNo == car.regNo
                                       select car).FirstOrDefault();

                    //if registration plate does not exist in database, add as a new car
                    if(existingCar == null)
                    {
                        tblCar newCar = new tblCar
                        {
                            regNo = regNo,
                            customerID = custID,
                            manufacturer = manufacturer,
                            model = model,
                            colour = colour
                        };

                        autoJDC.tblCars.InsertOnSubmit(newCar);
                        addCarResultMessage = $"Car with registration number {regNo} added to Customer {customerId}.";
                    }
                    else //registration plate exists, overwrite with input field values
                    {
                        existingCar.regNo = regNo;
                        existingCar.customerID = custID;
                        existingCar.manufacturer = manufacturer;
                        existingCar.model = model;
                        existingCar.colour = colour;

                        addCarResultMessage = $"Car with registration number {regNo} updated to Customer {existingCar.customerID}.";
                    }
                    
                    autoJDC.SubmitChanges();
                    
                }

                catch
                {
                    addCarResultMessage = $"Car with registration number {regNo} failed to save to Customer {customerId}.";
                }
            }
            
            return addCarResultMessage;
        }

        //method to get a list of existing cars to populate cbo
        public List<string> GetExistingCars()
        {
            List<string> carList = new List<string>();

            using (AutoJalopyL2SDataContext autoJDC = new AutoJalopyL2SDataContext())
            {
                var carsFromDB = from cars in autoJDC.tblCars
                                 orderby cars.regNo
                                 select cars.regNo;

                foreach (var car in carsFromDB)
                {
                    carList.Add(car.ToString());
                }
            }

            return carList;
        }

        //method to search and load selected car details based on reg plate
        public CarClass GetSelectedCar(string existingRegNo)
        {
            CarClass selectedCar;

            using (AutoJalopyL2SDataContext autoJDC = new AutoJalopyL2SDataContext())
            {
                var retrievedCar = (from cars in autoJDC.tblCars
                                    where existingRegNo == cars.regNo
                                    select cars).Single();

                selectedCar = new CarClass(retrievedCar.regNo, retrievedCar.customerID, retrievedCar.manufacturer, retrievedCar.model, retrievedCar.colour);
            }

            return selectedCar;
        }

        //method to get list of existing customers
        public List<string> GetExistingCustomers()
        {
            List<string> customerList = new List<string>();

            using (AutoJalopyL2SDataContext autoJDC = new AutoJalopyL2SDataContext())
            {
                var customersFromDB = from customers in autoJDC.tblCustomers
                                      let custFullName = $"{customers.customerID} {customers.firstName} {customers.lastName}"
                                      select custFullName;

                foreach(var customer in customersFromDB)
                {
                    customerList.Add(customer.ToString());
                }

            }

            return customerList;
        }

        //method to search for and load details of existing customer based on ID
        public CustomerClass GetSelectedCustomer(string customerID)
        {
            int existingCustomerID = int.Parse(customerID);
            CustomerClass selectedCustomer;

            using (AutoJalopyL2SDataContext autoJDC = new AutoJalopyL2SDataContext())
            {
                var retrievedCustomer = (from custs in autoJDC.tblCustomers
                                    where existingCustomerID == custs.customerID
                                    select custs).Single();

                selectedCustomer = new CustomerClass(retrievedCustomer.customerID, retrievedCustomer.firstName, retrievedCustomer.lastName, retrievedCustomer.customerAddress, retrievedCustomer.phone, retrievedCustomer.email, retrievedCustomer.bookingMethod);
            }

            return selectedCustomer;
        }

        //method to add a new customer
        public string AddCustomer(string fn, string ln, string addrss, string phn, string eml, string bkingMthd)
        {
            string addCustomerResultMessage = "";

            using (AutoJalopyL2SDataContext autoJDC = new AutoJalopyL2SDataContext())
            {
                try
                {
                    tblCustomer newCustomer = new tblCustomer
                    {
                        firstName = fn,
                        lastName = ln,
                        customerAddress = addrss,
                        phone = phn,
                        email = eml,
                        bookingMethod = bkingMthd
                    };

                    autoJDC.tblCustomers.InsertOnSubmit(newCustomer);
                    autoJDC.SubmitChanges();

                    addCustomerResultMessage = "New Customer record created.";
                }

                catch
                {
                    addCustomerResultMessage = "Customer record creation failed. Please ensure all fields are entered correctly.";
                }
            }
            
            return addCustomerResultMessage;
        }

        //method to update and overwrite customer details
        public string UpdateCustomer(string id, string fn, string ln, string addrss, string phn, string eml, string bkingMthd)
        {
            string updateCustomerMessage = "";

            using (AutoJalopyL2SDataContext autoJDC = new AutoJalopyL2SDataContext())
            {
                int custID = int.Parse(id);

                try
                {
                    var existingCustomer = (from custs in autoJDC.tblCustomers
                                             where custID == custs.customerID
                                             select custs).Single();

                    existingCustomer.firstName = fn;
                    existingCustomer.lastName = ln;
                    existingCustomer.customerAddress = addrss;
                    existingCustomer.phone = phn;
                    existingCustomer.email = eml;
                    existingCustomer.bookingMethod = bkingMthd;

                    updateCustomerMessage = $"Customer ID {custID} details updated.";

                    autoJDC.SubmitChanges();

                }

                catch
                {
                updateCustomerMessage = $"Customer ID {id} failed to update.";
                }
            }

            return updateCustomerMessage;
        }

        //method to get list of existing staff members who are mechanics
        public List<string> GetCurrentMechanics()
        {
            List<string> mechanicList = new List<string>();

            using (AutoJalopyL2SDataContext autoJDC = new AutoJalopyL2SDataContext())
            {
                var mechanicsFromDB = from staff in autoJDC.tblStaffs
                                      where staff.staffType == "Mechanic"
                                      let mechanicName = $"{staff.staffID} {staff.firstName} {staff.lastName}"
                                      select mechanicName;

                foreach (var mechanic in mechanicsFromDB)
                {
                    mechanicList.Add(mechanic.ToString());
                }

            }

            return mechanicList;
        }

        //method to save a new job booking
        public string SaveJob (int jobID, string regNo, string mechanic, DateTime startDate, DateTime finishDate)
        {
            string addJobResultMessage = "";
            int mechanicID = int.Parse(mechanic);

            using (AutoJalopyL2SDataContext autoJDC = new AutoJalopyL2SDataContext())
            {
                try
                {
                    //method to get job name/type based on selection
                    var jobName = (from jobs in autoJDC.tblJobs
                                   where jobs.jobID == jobID
                                   select jobs.jobType).Single();


                        tblBooking newJob = new tblBooking
                        {
                            jobID = jobID,
                            regNo = regNo,
                            startDate = startDate,
                            finishDate = finishDate,
                            staffID = mechanicID
                        };

                    autoJDC.tblBookings.InsertOnSubmit(newJob);

                    addJobResultMessage = $"Car {regNo} is booked for a {jobName} from {startDate.ToShortDateString()} to {finishDate.ToShortDateString()}";

                    autoJDC.SubmitChanges();
                }

                catch
                {
                    addJobResultMessage = "Booking failed. Please ensure all fields are entered correctly.";
                }
            }

            return addJobResultMessage;
        }

        //method to update a selected job booking
        public string UpdateBooking(string bookingID, int jobID, string regNo, int mechanic, DateTime startDate, DateTime finishDate)
        {
            string updateJobResultMessage = "";
            int bookingIDNo = int.Parse(bookingID);

            using (AutoJalopyL2SDataContext autoJDC = new AutoJalopyL2SDataContext())
            {
                try
                {
                    var bookingToUpdate = (from bookings in autoJDC.tblBookings
                                           where bookingIDNo == bookings.bookingID
                                           select bookings).Single();

                    bookingToUpdate.bookingID = int.Parse(bookingID);
                    bookingToUpdate.jobID = jobID;
                    bookingToUpdate.regNo = regNo;
                    bookingToUpdate.staffID = mechanic;
                    bookingToUpdate.startDate = startDate;
                    bookingToUpdate.finishDate = finishDate;

                    autoJDC.SubmitChanges();

                    updateJobResultMessage = $"Booking {bookingToUpdate.bookingID} updated.";
                }

                catch
                {
                    updateJobResultMessage = $"Booking failed to update.";
                }

                return updateJobResultMessage;
            }
        }

        //method to get list of booked jobs in database
        public List<string> GetExistingBookings()
        {
            List<string> bookingsList = new List<string>();

            using (AutoJalopyL2SDataContext autoJDC = new AutoJalopyL2SDataContext())
            {
                var bookingsFromDB = from bookings in autoJDC.tblBookings
                                     join jobName in autoJDC.tblJobs
                                     on bookings.jobID equals jobName.jobID
                                     let bookingDets = $"{bookings.bookingID} {jobName.jobType} {bookings.regNo}"
                                     select bookingDets;

                foreach (var booking in bookingsFromDB)
                {
                    bookingsList.Add(booking.ToString());
                }

            }

            return bookingsList;
        }

        //method to search for and load job booking details based on job ID
        public Bookings GetSelectedBooking(string existingBooking)
        {
            Bookings selectedBooking = null;

            int existingBookingID;
            int.TryParse(existingBooking, out existingBookingID);

            if(existingBookingID > 0)
            {
                using (AutoJalopyL2SDataContext autoJDC = new AutoJalopyL2SDataContext())
                {
                    var retrievedBooking = (from booking in autoJDC.tblBookings
                                            where existingBookingID == booking.bookingID
                                            select booking).Single();

                    selectedBooking = new Bookings(retrievedBooking.bookingID, retrievedBooking.jobID, retrievedBooking.regNo, retrievedBooking.startDate, retrievedBooking.finishDate, retrievedBooking.staffID);
                }
            }

            return selectedBooking;
        }

        //method to delete a booking based on selected job ID
        public string DeleteBooking(string bookingID)
        {
            string deleteBookingMessage = "";

            int existingBookingID = int.Parse(bookingID);

            using (AutoJalopyL2SDataContext autoJDC = new AutoJalopyL2SDataContext())
            {
                try
                {
                    var bookingToDelete = (from booking in autoJDC.tblBookings
                                            where existingBookingID == booking.bookingID
                                            select booking).Single();

                    autoJDC.tblBookings.DeleteOnSubmit(bookingToDelete);
                    autoJDC.SubmitChanges();

                    deleteBookingMessage = "Booking deleted.";
                }

                catch
                {
                    deleteBookingMessage = "Booking deletion failed.";
                }

                return deleteBookingMessage;
            }
        }

        //method to get list of customers and all their details
        public List<object> GetCustomerList()
        {
            List<object> customerList = new List<object>();
            
                using(AutoJalopyL2SDataContext autoJDC = new AutoJalopyL2SDataContext())
                {
                    var allCustomers = (from customers in autoJDC.tblCustomers
                                       select new
                                       {
                                            CustomerID = customers.customerID,
                                            Name = ($"{customers.firstName} {customers.lastName}"),
                                            Address = customers.customerAddress,
                                            Phone = customers.phone,
                                            Email = customers.email,
                                            Booking = customers.bookingMethod
                                       }).ToList();

                    foreach (var customer in allCustomers)
                    {
                        customerList.Add(customer);
                    }
                }

            return customerList;
        }

        //methods to get a detailed list of customers and their cars
        public List<object> GetCustomerCarList()
        {
            List<object> customerCarList = new List<object>();

            using (AutoJalopyL2SDataContext autoJDC = new AutoJalopyL2SDataContext())
            {
                var allCustomerCars = (from customers in autoJDC.tblCustomers
                                       join cars in autoJDC.tblCars
                                       on customers.customerID equals cars.customerID
                                       select new
                                       {
                                           Car = cars.regNo,
                                           CustomerID = customers.customerID,
                                           Name = ($"{customers.firstName} {customers.lastName}")
                                       }).ToList();

                foreach (var car in allCustomerCars)
                {
                    customerCarList.Add(car);
                }
            }

            return customerCarList;
        }

        //method to show all job bookings
        public List<object> ShowAllBookings()
        {
            List<object> bookingsList = new List<object>();

            using (AutoJalopyL2SDataContext autoJDC = new AutoJalopyL2SDataContext())
            {
                var bookingDeets = (from bookings in autoJDC.tblBookings
                                    join car in autoJDC.tblCars
                                    on bookings.regNo equals car.regNo
                                    join customer in autoJDC.tblCustomers
                                    on car.customerID equals customer.customerID
                                    join mechanic in autoJDC.tblStaffs
                                    on bookings.staffID equals mechanic.staffID
                                    join job in autoJDC.tblJobs
                                    on bookings.jobID equals job.jobID
                                    select new
                                   {
                                       BookingID = bookings.bookingID,
                                       Customer = ($"{customer.firstName} {customer.lastName}"),
                                       Car = bookings.regNo,
                                       Job = job.jobType,
                                       Mechanic = ($"{mechanic.firstName} {mechanic.lastName}"),
                                       EstimatedCompletion = bookings.finishDate
                                   }).ToList();

                foreach (var booking in bookingDeets)
                {
                    bookingsList.Add(booking);
                }
            }

            return bookingsList;
        }

        //method to get list of mechanics working on any jobs in the next two weeks, regardless of start and finish dates
        public List<object> Get2WeekJobs(string mechanicID)
        {
            List<object> jobsList = new List<object>();

            using (AutoJalopyL2SDataContext autoJDC = new AutoJalopyL2SDataContext())
            {
                var jobs2Weeks = (from bookings in autoJDC.tblBookings
                                  join mechanics in autoJDC.tblStaffs
                                  on bookings.staffID equals mechanics.staffID
                                  join jobs in autoJDC.tblJobs
                                  on bookings.jobID equals jobs.jobID
                                  where (bookings.startDate < DateTime.Now.AddDays(14D) && DateTime.Now < bookings.finishDate)
                                  where mechanics.staffID == int.Parse(mechanicID)
                                  select new
                                  {
                                      Job = jobs.jobType,
                                      Start = bookings.startDate,
                                      Finish = bookings.finishDate
                                 }).ToList();
                
                foreach (var job in jobs2Weeks)
                {
                    jobsList.Add(job);
                }
            }

            return jobsList;
        }
    }
}
