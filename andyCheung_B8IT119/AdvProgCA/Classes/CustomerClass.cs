﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace AdvProgCA.Classes
{
    //inhereits from Person class
    public class CustomerClass : Person
    {
        [Display(Name = "Booking Method")]
        public BookingMethods BookingMethod { get; set; }
        //uses contructor with parameters
        public CustomerClass(int id, string fname, string lname, string address, string phone, string email, string bookMethod)
        {
            ID = id;
            FName = fname;
            LName = lname;
            Address = address;
            Phone = phone;
            Email = email;
            BookingMethod = (BookingMethods)Enum.Parse(typeof(BookingMethods), bookMethod);
        }
    }
}
