﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdvProgCA.Classes
{
    public class CarClass
    {
        //any car to instantiate must use constructor with parameters
        public CarClass(string regNo, int customerID, string manufacturer, string model, string colour)
        {
            RegNo = regNo;
            CustomerID = customerID;
            Manufacturer = manufacturer;
            Model = model;
            Colour = (Classes.CarColour)Enum.Parse(typeof(Classes.CarColour), colour);
        }

        public string RegNo { get; set; }

        public int CustomerID { get; set; }

        public string Manufacturer { get; set; }

        public string Model { get; set; }

        public CarColour Colour { get; set; }
    }
}
