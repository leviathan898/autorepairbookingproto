﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdvProgCA.Classes
{
    class Jobs
    {
        public JobTypes JobType { get; set; }

        public decimal Cost { get; set; }
    }
}
