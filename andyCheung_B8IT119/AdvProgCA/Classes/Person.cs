﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace AdvProgCA.Classes
{
    public abstract class Person
    {
        public int ID { get; set; }

        [Display(Name = "First Name")]
        public string FName { get; set; }

        [Display(Name = "Last Name")]
        public string LName { get; set; }
        
        public string Address { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }
    }
}
