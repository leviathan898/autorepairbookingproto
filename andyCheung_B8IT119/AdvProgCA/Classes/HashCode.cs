﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace AdvProgCA.Classes
{
    class HashCode
    {
        //SHA1 encryption for password
        public string PassHash(string password)
        {
            SHA1 sha = SHA1.Create();

            byte[] passwordHash = sha.ComputeHash(Encoding.Default.GetBytes(password));

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < passwordHash.Length; i++)
            {
                sb.Append(passwordHash[i].ToString());
            }

            return sb.ToString();
        }
    }
}
