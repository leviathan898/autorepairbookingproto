﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdvProgCA.Classes
{
    class Bookings
    {
        public int BookingID { get; set; }

        public int JobID { get; set; }

        public string RegNo { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime FinishDate { get; set; }

        public int StaffID { get; set; }

        //any booking instantiated must have properties enclosed
        public Bookings(int bookingID, int jobID, string regNo, DateTime startDate, DateTime finishDate, int staffID)
        {
            BookingID = bookingID;
            JobID = jobID;
            RegNo = regNo;
            StartDate = startDate;
            FinishDate = finishDate;
            StaffID = staffID;
        }
    }
}
