﻿namespace AdvProgCA
{
    partial class AutoJalopyMotors
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnLogout = new System.Windows.Forms.Button();
            this.btnCustomerCarDetails = new System.Windows.Forms.Button();
            this.btnJobDetails = new System.Windows.Forms.Button();
            this.btnAdmin = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnLogout
            // 
            this.btnLogout.Location = new System.Drawing.Point(61, 210);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(157, 23);
            this.btnLogout.TabIndex = 4;
            this.btnLogout.Text = "Logout";
            this.btnLogout.UseVisualStyleBackColor = true;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // btnCustomerCarDetails
            // 
            this.btnCustomerCarDetails.Location = new System.Drawing.Point(61, 30);
            this.btnCustomerCarDetails.Name = "btnCustomerCarDetails";
            this.btnCustomerCarDetails.Size = new System.Drawing.Size(157, 23);
            this.btnCustomerCarDetails.TabIndex = 1;
            this.btnCustomerCarDetails.Text = "Customer Details";
            this.btnCustomerCarDetails.UseVisualStyleBackColor = true;
            this.btnCustomerCarDetails.Click += new System.EventHandler(this.btnCarDetails_Click);
            // 
            // btnJobDetails
            // 
            this.btnJobDetails.Location = new System.Drawing.Point(61, 90);
            this.btnJobDetails.Name = "btnJobDetails";
            this.btnJobDetails.Size = new System.Drawing.Size(157, 23);
            this.btnJobDetails.TabIndex = 2;
            this.btnJobDetails.Text = "Bookings";
            this.btnJobDetails.UseVisualStyleBackColor = true;
            this.btnJobDetails.Click += new System.EventHandler(this.btnJobDetails_Click);
            // 
            // btnAdmin
            // 
            this.btnAdmin.Location = new System.Drawing.Point(61, 150);
            this.btnAdmin.Name = "btnAdmin";
            this.btnAdmin.Size = new System.Drawing.Size(157, 23);
            this.btnAdmin.TabIndex = 3;
            this.btnAdmin.Text = "Administration";
            this.btnAdmin.UseVisualStyleBackColor = true;
            this.btnAdmin.Click += new System.EventHandler(this.btnAdmin_Click);
            // 
            // AutoJalopyMotors
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.ControlBox = false;
            this.Controls.Add(this.btnAdmin);
            this.Controls.Add(this.btnJobDetails);
            this.Controls.Add(this.btnCustomerCarDetails);
            this.Controls.Add(this.btnLogout);
            this.Name = "AutoJalopyMotors";
            this.Text = "AutoJalopy Motors";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.Button btnCustomerCarDetails;
        private System.Windows.Forms.Button btnJobDetails;
        private System.Windows.Forms.Button btnAdmin;
    }
}