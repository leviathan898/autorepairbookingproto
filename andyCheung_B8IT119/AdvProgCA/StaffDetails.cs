﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Configuration;

namespace AdvProgCA
{
    public partial class StaffDetails : Form
    {
        string noDigitsRegex = ConfigurationManager.AppSettings["NoDigits"];
        string mobileRegex = ConfigurationManager.AppSettings["MobileRegEx"];
        string emailRegex = ConfigurationManager.AppSettings["EmailRegex"];
        MyMethods mm = new MyMethods();

        public StaffDetails()
        {
            InitializeComponent();
            this.CenterToScreen();
            cboStaffType.DataSource = Enum.GetValues(typeof(Classes.StaffTypes));

            //if no staff records exist, initial run of program opens staff creation page and limits staff record creation to admin (one admin must exist in database system)
            if (!(mm.checkForExistingStaff()))
            {
                cboStaffType.SelectedIndex = 2;
                cboStaffType.Enabled = false;
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAddStaff_Click(object sender, EventArgs e)
        {
            //hash password and store encrypted password to database
            Classes.HashCode hc = new Classes.HashCode();

            string fName = txtFName.Text;
            string lName = txtLName.Text;
            string address = txtAddress.Text;
            string phone = txtPhone.Text;
            string email = txtEmail.Text;
            string passwordHash = hc.PassHash(txtPassword.Text);
            string staffType = cboStaffType.Text;

            if (!(Regex.IsMatch(fName, noDigitsRegex) || Regex.IsMatch(lName, noDigitsRegex)))
            {
                MessageBox.Show("Invalid Name inputs. Only non-numbers are allowed");
            }
            else if (!(Regex.IsMatch(phone, mobileRegex)))
            {
                MessageBox.Show("Invalid Contact Information. Only numbers are allowed for phone.");
            }
            else
            {
                MessageBox.Show(mm.AddStaff(fName, lName, address, phone, email, passwordHash, staffType));
            }

            txtFName.Clear();
            txtLName.Clear();
            txtAddress.Clear();
            txtPhone.Clear();
            txtEmail.Clear();
            txtPassword.Clear();
            cboStaffType.Enabled = true;
        }
    }
}
