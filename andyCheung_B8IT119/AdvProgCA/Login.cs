﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;
using AdvProgCA.Classes;

namespace AdvProgCA
{
    public delegate void eventDelegate();

    public partial class Login : Form
    {
        public event eventDelegate logIn;

        public Login()
        {
            InitializeComponent();
            this.CenterToScreen();

            //check for staff in database
            using (AutoJalopyL2SDataContext autoJDataContext = new AutoJalopyL2SDataContext())
            {
                var checkForStaff = (from staffMembers in autoJDataContext.tblStaffs
                                     select staffMembers);

                //if no staff records exist, open with create staff form
                if (checkForStaff.Any() == false)
                {
                    using (StaffDetails staffForm = new StaffDetails())
                    {
                        staffForm.ShowDialog();
                    }
                }
            }
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            HashCode hc = new HashCode();
            MyMethods mm = new MyMethods();

            string staffID = txtStaffID.Text;
            string password = hc.PassHash(txtPassword.Text);
            bool authentication;
            StaffTypes staffType;

            //check for matching staff ID and hashed password in database, return bool and stafftype
            mm.Login(staffID, password, out authentication, out staffType);
            
            //if staff exists(true)
            if(authentication)
            {
                //if staff is an admin, fire event
                if (staffType == StaffTypes.Admin)
                {
                    logIn();
                }

                this.Close();

            }
            else
            {
                MessageBox.Show("Invalid Staff ID and Password.");
            }
            
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
