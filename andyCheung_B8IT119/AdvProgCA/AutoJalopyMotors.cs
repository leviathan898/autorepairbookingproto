﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdvProgCA
{

    public partial class AutoJalopyMotors : Form
    {
        public AutoJalopyMotors()
        {
            InitializeComponent();
            this.CenterToScreen();
            btnAdmin.Enabled = false;

            //this page loads on program start, but forces login page pop up
            Login loginForm = new Login();
            //subscribes method enableAdminBtn to event delegate
            loginForm.logIn += new eventDelegate(enableAdminBtn);
            loginForm.ShowDialog();
        }

        //this method runs if the logIn event is fired from login form
        void enableAdminBtn()
        {
            btnAdmin.Enabled = true;
        }

        private void btnCarDetails_Click(object sender, EventArgs e)
        {
            using (CustomerDetails carForm = new CustomerDetails())
            {
                carForm.ShowDialog();
            }
        }

        private void btnJobDetails_Click(object sender, EventArgs e)
        {
            using (BookingDetails jobForm = new BookingDetails())
            {
                jobForm.ShowDialog();
            }
        }

        private void btnAdmin_Click(object sender, EventArgs e)
        {
            using (Admin adminForm = new Admin())
            {
                adminForm.ShowDialog();
            }
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            btnAdmin.Enabled = false;

            using (Login loginForm = new Login())
            {
                //creates new login form when logging out, subscribes the enableAdminBtn to this new form
                loginForm.logIn += new eventDelegate(enableAdminBtn);
                loginForm.ShowDialog();
            }
        }
    }
}
