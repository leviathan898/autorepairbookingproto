﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdvProgCA
{
    public partial class Admin : Form
    {
        public Admin()
        {
            InitializeComponent();
            this.CenterToScreen();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnStaffDetails_Click(object sender, EventArgs e)
        {
            using (StaffDetails staffForm = new StaffDetails())
            {
                staffForm.ShowDialog();
            }
        }

        private void btnReports_Click(object sender, EventArgs e)
        {
            using (Reports reportForm = new Reports())
            {
                reportForm.ShowDialog();
            }
        }
    }
}
