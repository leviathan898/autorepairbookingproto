﻿namespace AdvProgCA
{
    partial class Reports
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvDisplay = new System.Windows.Forms.DataGridView();
            this.btnTwoWeeks = new System.Windows.Forms.Button();
            this.btnJobs = new System.Windows.Forms.Button();
            this.btnCustomerCars = new System.Windows.Forms.Button();
            this.lblMechanic = new System.Windows.Forms.Label();
            this.cboMechanic = new System.Windows.Forms.ComboBox();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnShowAllCustomers = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDisplay)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvDisplay
            // 
            this.dgvDisplay.AllowUserToAddRows = false;
            this.dgvDisplay.AllowUserToDeleteRows = false;
            this.dgvDisplay.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDisplay.Location = new System.Drawing.Point(12, 12);
            this.dgvDisplay.Name = "dgvDisplay";
            this.dgvDisplay.Size = new System.Drawing.Size(552, 199);
            this.dgvDisplay.TabIndex = 0;
            // 
            // btnTwoWeeks
            // 
            this.btnTwoWeeks.Location = new System.Drawing.Point(433, 258);
            this.btnTwoWeeks.Name = "btnTwoWeeks";
            this.btnTwoWeeks.Size = new System.Drawing.Size(124, 25);
            this.btnTwoWeeks.TabIndex = 5;
            this.btnTwoWeeks.Text = "Jobs Next 14 Days";
            this.btnTwoWeeks.UseVisualStyleBackColor = true;
            this.btnTwoWeeks.Click += new System.EventHandler(this.btnTwoWeeks_Click);
            // 
            // btnJobs
            // 
            this.btnJobs.Location = new System.Drawing.Point(203, 258);
            this.btnJobs.Name = "btnJobs";
            this.btnJobs.Size = new System.Drawing.Size(124, 23);
            this.btnJobs.TabIndex = 3;
            this.btnJobs.Text = "Current Jobs";
            this.btnJobs.UseVisualStyleBackColor = true;
            this.btnJobs.Click += new System.EventHandler(this.btnJobs_Click);
            // 
            // btnCustomerCars
            // 
            this.btnCustomerCars.Location = new System.Drawing.Point(203, 229);
            this.btnCustomerCars.Name = "btnCustomerCars";
            this.btnCustomerCars.Size = new System.Drawing.Size(124, 23);
            this.btnCustomerCars.TabIndex = 2;
            this.btnCustomerCars.Text = "Customer Cars";
            this.btnCustomerCars.UseVisualStyleBackColor = true;
            this.btnCustomerCars.Click += new System.EventHandler(this.btnCustomerCars_Click);
            // 
            // lblMechanic
            // 
            this.lblMechanic.AutoSize = true;
            this.lblMechanic.Location = new System.Drawing.Point(353, 234);
            this.lblMechanic.Name = "lblMechanic";
            this.lblMechanic.Size = new System.Drawing.Size(60, 13);
            this.lblMechanic.TabIndex = 60;
            this.lblMechanic.Text = "Mechanic :";
            // 
            // cboMechanic
            // 
            this.cboMechanic.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboMechanic.FormattingEnabled = true;
            this.cboMechanic.Location = new System.Drawing.Point(424, 231);
            this.cboMechanic.Name = "cboMechanic";
            this.cboMechanic.Size = new System.Drawing.Size(140, 21);
            this.cboMechanic.TabIndex = 4;
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(12, 292);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(58, 23);
            this.btnBack.TabIndex = 6;
            this.btnBack.Text = "<< Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnShowAllCustomers
            // 
            this.btnShowAllCustomers.Location = new System.Drawing.Point(12, 229);
            this.btnShowAllCustomers.Name = "btnShowAllCustomers";
            this.btnShowAllCustomers.Size = new System.Drawing.Size(124, 23);
            this.btnShowAllCustomers.TabIndex = 1;
            this.btnShowAllCustomers.Text = "Customer List";
            this.btnShowAllCustomers.UseVisualStyleBackColor = true;
            this.btnShowAllCustomers.Click += new System.EventHandler(this.btnShowAllCustomers_Click);
            // 
            // Reports
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(576, 327);
            this.ControlBox = false;
            this.Controls.Add(this.btnShowAllCustomers);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.lblMechanic);
            this.Controls.Add(this.cboMechanic);
            this.Controls.Add(this.btnCustomerCars);
            this.Controls.Add(this.btnJobs);
            this.Controls.Add(this.btnTwoWeeks);
            this.Controls.Add(this.dgvDisplay);
            this.Name = "Reports";
            this.Text = "Reports";
            ((System.ComponentModel.ISupportInitialize)(this.dgvDisplay)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvDisplay;
        private System.Windows.Forms.Button btnTwoWeeks;
        private System.Windows.Forms.Button btnJobs;
        private System.Windows.Forms.Button btnCustomerCars;
        private System.Windows.Forms.Label lblMechanic;
        private System.Windows.Forms.ComboBox cboMechanic;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnShowAllCustomers;
    }
}