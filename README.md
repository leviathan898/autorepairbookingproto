Two week prototype of a partial booking system for an auto repair shop case study. Client requested booking suite with CRUD functionality (customers, cars, bookings), authentication (admin vs. non-admin) and database integration (LINQ to SQL). Database design and implementation was done first, followed by front-end design and then coding of interactivity and functionality.

Because of the local database copying to the debug file, in order to view any records made while running the debug you have to first run the debug once (in order to create the debug copy of the database), stop debugging, then add a new connection to the database located in AdvProgCA/bin/Debug/AutoJalopy.mdf. This should be added with the name AutoJalopy.mdf1 in the Server Explorer, and it will be this database that is used during debugging.

As to the project, the brief said to forgo CRUD functionality for Staff but I wanted to test myself and incorporate an Add Staff function, with different staff types. If no staff exists, the program will automatically go to the Add Staff page on initiation, with a requirement that the first staff created must be an admin. Upon addition of an admin staff record, subsequent runs of the program will prompt the login page.

I have also included Stored Procedures for Creating Tables, Inserting Jobs as well as Resetting the Identity values of auto-incrementing Primary Keys in tables.

-leviathan898